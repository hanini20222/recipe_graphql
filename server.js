const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { graphiqlExpress, graphqlExpress} = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');

require('dotenv').config({ path: 'variables.env' })


const User = require('./models/User');
const Category = require('./models/Category');
const Recipe = require('./models/Recipe');

const { typeDefs } = require('./schema');
const { resolvers } = require('./resolvers');

const schema = makeExecutableSchema({
  typeDefs, resolvers
});

mongoose
  .connect('mongodb://localhost:27017/recipes')
  .then(() => console.log('Db Connected...'))
  .catch(err=> console.log(err));

const app = express();

app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));
app.use('/graphql',
  bodyParser.json(), 
  graphqlExpress({
    schema,
    context: { Recipe, User, Category }
}));

const PORT = process.env.PORT || 4444;

app.listen(PORT, () => { console.log(`Server running on ${PORT}`) })
