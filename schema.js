exports.typeDefs = `

type Recipe {
  name: String!
  category: String!
  description: String!
  instructions: String!
  likes: Int
  username: String!
}

type User {
  username: String!
  email: String!
  password: String!
  favorites: [Recipe]
}

type Category {
  name: String!
  description: String
}

type Query {
  getAllRecipes: [Recipe]
}

`;